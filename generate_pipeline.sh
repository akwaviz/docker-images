#!/bin/sh

cat <<EOF
image: docker:stable

workflow:
  rules:
    - if: \$CI_MERGE_REQUEST_IID
    - if: \$CI_COMMIT_BRANCH

stages:
  - build
  - stop

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_TLS_CERTDIR: ""
  DOCKER_DRIVER: overlay2
  CONTAINER_RELEASE_PATH: registry.gitlab.com/inendi/docker-images

before_script:
  - docker login -u gitlab-ci-token --password \$CI_JOB_TOKEN registry.gitlab.com

EOF

if [ $REBUILD_ALL -eq 1 ]; then
    MODIFIED_DOCKERFILES=`find * -type f -wholename "*_docker/Dockerfile"`
else
  git fetch -a
  COMMIT_BEFORE="origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
  if [ -z $CI_MERGE_REQUEST_TARGET_BRANCH_NAME ]; then
    COMMIT_BEFORE=$CI_COMMIT_BEFORE_SHA
  fi
  MODIFIED_DOCKERFILES=`git diff-tree --no-commit-id --name-only -r $COMMIT_BEFORE -r $CI_COMMIT_SHA | grep "_docker/Dockerfile"`
fi

for dockerfile in $MODIFIED_DOCKERFILES
do
if [ ! -f $dockerfile ]; then
    continue # Ignore deleted files
fi
job_folder=`echo $dockerfile | cut -d"/" -f 1`
job_name="${job_folder%_docker}"
cat <<EOF

${job_name}:
  stage: build
  tags:
    - gitlab-org-docker
  script:
    - cd ${job_folder}
    - docker build --pull -t \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_SHORT_SHA  .
    - docker tag \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_SHORT_SHA \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_REF_NAME
    - docker push \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_REF_NAME
    - docker push \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_SHORT_SHA
  environment:
    on_stop: ${job_name}_stop
    name: review/\$CI_COMMIT_REF_SLUG
    url: https://\$CI_MERGE_REQUEST_PROJECT_URL

${job_name}_stop: # Delete docker image when branch is merged
  stage: stop
  tags:
   - gitlab-org-docker
  script:
    - docker image rm \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_REF_NAME
    - docker image rm \$CONTAINER_RELEASE_PATH/${job_name}:\$CI_COMMIT_SHORT_SHA
  environment:
    name: review/\$CI_COMMIT_REF_SLUG
    action: stop
  when: manual

EOF
done
